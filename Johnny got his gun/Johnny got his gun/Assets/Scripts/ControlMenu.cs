using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenu : MonoBehaviour
{

    public void JUGAR()
    {
        SceneManager.LoadScene("Escena Eye");
    }

    public void MENU()
    {
        SceneManager.LoadScene("Menu");
    }

    public void SALIR()
    {
        Debug.Log("Saliste del juego :)");
        Application.Quit();
    }

}
