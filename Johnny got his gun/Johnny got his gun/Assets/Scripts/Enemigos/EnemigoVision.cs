using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoVision : MonoBehaviour
{
    private float velocidadInicial;
    private void Start()
    {
        velocidadInicial = GetComponent<Enemigomovimiento>().Velocidad;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Light"))
        {
            GetComponent<Enemigomovimiento>().Velocidad = 0;
            print("luz");
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Light"))
        {
            GetComponent<Enemigomovimiento>().Velocidad = 40;
            print("no luz");
        }
    }

}
