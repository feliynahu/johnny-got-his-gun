using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigomovimiento : MonoBehaviour
{
    [SerializeField] private float velocidad;
    [SerializeField] private Transform[] puntos;
    [SerializeField] private float DistanciaMinima;
    private int rnd;

    public float Velocidad { get => velocidad; set => velocidad = value; }

    void Start()
    {
        rnd = Random.Range(0, puntos.Length);

    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, puntos[rnd].position, Velocidad * Time.deltaTime);
        if(Vector2.Distance(transform.position,puntos[rnd].position) < DistanciaMinima)
        {
            rnd = Random.Range(0, puntos.Length);
        }
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if(collision.gameObject.name.Substring(0,6)=="Player")
    //    Destroy(collision.gameObject);
    //}
}
