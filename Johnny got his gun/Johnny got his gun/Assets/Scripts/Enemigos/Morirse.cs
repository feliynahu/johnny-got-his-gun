using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Morirse : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Substring(0, 6) == "Player")
        {
            if (collision.gameObject.CompareTag("Tacto"))
            {
                if (collision.GetContact(0).normal.y <= -0.9)
                {
                    collision.gameObject.GetComponent<Rebote>().Rebotar();
                    Destroy(gameObject);
                }
                else Destroy(collision.gameObject);
            }
            else Destroy(collision.gameObject); 
        }
    }
}

    //private GameObject obj;
    //private void Start()
    //{
    //    obj = GetComponentInParent<Enemigomovimiento>().gameObject;
    //}
    //private void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (other.CompareTag("Tacto"))
    //    {
    //        Destroy(gameObject);
    //    }
    //    else { Destroy(obj); }
    //}

