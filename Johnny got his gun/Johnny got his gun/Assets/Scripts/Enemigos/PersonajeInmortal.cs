using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeInmortal : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Kill"))
        {
            Destroy(gameObject);
        }
        else if(collision.gameObject.name.Substring(0, 6) == "Player")
        {
            Destroy(collision.gameObject);
        }
    }
}
