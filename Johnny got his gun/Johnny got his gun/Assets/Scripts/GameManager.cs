using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    CinemachineVirtualCamera Camera;
    [SerializeField] List<GameObject> Personajes;
    [SerializeField] List<GameObject> Enemigos;
    [SerializeField] int contadorEnemigos;
    int numeroJugador;
    void Start()
    {
        contadorEnemigos = Enemigos.Count;
        numeroJugador = 0;
        Camera = FindObjectOfType<CinemachineVirtualCamera>();
        Camera.Follow = Personajes[numeroJugador].transform;
        foreach (GameObject pj in Personajes)
        {
            pj.GetComponent<ControlPj>().enabled = false;
        }
        Personajes[numeroJugador].GetComponent<ControlPj>().enabled = true;
    }

    void Update()
    {
        CambioDePersonaje();
        ContarCantidadEnemigos();
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(1);
    }

    public void CambioDePersonaje()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            for (int x = Personajes.Count - 1; x > -1; x--)
            {
                if (Personajes[x] == null)
                {
                    Personajes.RemoveAt(x);
                }
            }
            if (numeroJugador < Personajes.Count)
            {
                Camera.Follow = Personajes[numeroJugador].transform;
                foreach (GameObject pj in Personajes)
                {
                    pj.GetComponent<ControlPj>().enabled = false;
                    pj.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                }
                Personajes[numeroJugador].GetComponent<ControlPj>().enabled = true;
                Personajes[numeroJugador].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                numeroJugador++;
            }
            else
            {
                numeroJugador = 0;
                Camera.Follow = Personajes[numeroJugador].transform;
                foreach (GameObject pj in Personajes)
                {
                    pj.GetComponent<ControlPj>().enabled = false;
                    pj.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                }
                Personajes[numeroJugador].GetComponent<ControlPj>().enabled = true;
                Personajes[numeroJugador].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            }
        }
    }
    public void ContarCantidadEnemigos()
    {
        for (int i = Enemigos.Count - 1; i > -1; i--)
        {
            if (Enemigos[i] == null)
            {
                Enemigos.RemoveAt(i);
            }
        }
        if (Enemigos.Count == 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}
