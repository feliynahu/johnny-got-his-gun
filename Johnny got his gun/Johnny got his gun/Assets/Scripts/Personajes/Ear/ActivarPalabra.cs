using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarPalabra : MonoBehaviour
{
    public void Activarse()
    {
        GetComponent<MeshRenderer>().enabled = true;
        GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
    }
}
