using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ear : MonoBehaviour
{
    [SerializeField] private float radio;
    [SerializeField] ParticleSystem particle;
    void Start()
    {
        
    }

    void Update()
    {
        if (GetComponentInParent<ControlPj>().isActiveAndEnabled)
        {
            if (Input.GetKeyDown(KeyCode.E)) Escuchar();
        }
    }
    public void Escuchar()
    {
        particle.Play();
        Collider2D[] objetos = Physics2D.OverlapCircleAll(transform.position, radio);

        foreach (Collider2D colision in objetos)
        {
            ActivarPalabra palabras = colision.GetComponent<ActivarPalabra>();
            if (palabras != null)
            {
                palabras.Activarse();
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radio);
    }
}
