using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class BloquePalabra : MonoBehaviour
{
    [SerializeField] private string palabra;
    [SerializeField] private char[] palabraArray;
    [SerializeField] private char[] arrayTxt;
    private List<char> palabraUsar;
    [SerializeField] private TextMeshPro txt;
    private bool act;
    List<KeyCode> listaKeyCodes;
    char teclaApretada;

    public bool Act { get => act; set => act = value; }

    void Start()
    {
        txt = GetComponentInChildren<TextMeshPro>();
        listaKeyCodes = new List<KeyCode>();
        palabraUsar = new List<char>();
        ConvertirPalabra(palabra, ref palabraArray);
        ConvertirPalabra(txt.text, ref arrayTxt);
        Comparador(arrayTxt, palabraArray, ref palabraUsar);
        CambiarLetrasaKeyCode(palabraUsar, ref listaKeyCodes);

    }

    void Update()
    {
        
            if (Act)
            {
                CambiarPalabra();
            }
            RealizarFuncion();
        
    }
    private void CambiarPalabra()
    {
        teclaApretada = DevolverTecla();
        if (teclaApretada != ' ')
        {
            for (int i = 0; i < palabraArray.Length; i++)
            {
                if (palabraArray[i] == teclaApretada)
                {
                    arrayTxt[i] = teclaApretada;
                    string palabraAgregada = new string(arrayTxt);
                    txt.text = palabraAgregada;
                }
            }
        }
    }
    private void ConvertirPalabra(string palabra, ref char[] array)
    {
        array = palabra.ToCharArray();

    }
    public List<KeyCode> CambiarLetrasaKeyCode(List<char> letras, ref List<KeyCode> lista)
    {
        foreach (char letra in letras)
        {
            string word = letra.ToString();
            KeyCode tecla = (KeyCode)Enum.Parse(typeof(KeyCode), word);
            lista.Add(tecla);
        }
        return lista;
    }
    private void Comparador(char[] palabraGuiones, char[] palabraSinGuiones, ref List<char> listaPalabrasFinales)
    {
        for (int i = 0; i < palabraGuiones.Length; i++)
        {
            if (palabraGuiones[i] != palabraSinGuiones[i])
            {
                listaPalabrasFinales.Add(palabraSinGuiones[i]);
            }
        }
    }
    private char DevolverTecla()
    {
        foreach (KeyCode kcode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
            {
                foreach (KeyCode key in listaKeyCodes)
                {
                    if (kcode == key)
                    {
                        return char.ToUpper(Convert.ToChar(kcode));
                    }
                }
            }
        }
        return ' ';
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Mouth"))
        {
            GetComponent<SpriteRenderer>().color = Color.green;
            Act = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        GetComponent<SpriteRenderer>().color = Color.white;
        Act = false;
    }

    private void RealizarFuncion()
    {
        if (txt.text == "FALL")
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
        if (txt.text == "KILL")
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
    }

}
