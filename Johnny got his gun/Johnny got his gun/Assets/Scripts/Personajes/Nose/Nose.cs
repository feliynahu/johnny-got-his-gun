using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nose : MonoBehaviour
{
    private GameObject onda;
    private Transform transformInicial;
    SpriteRenderer sprite;

    void Start()
    {
        transformInicial = transform;

    }

    void Update()
    {
        if (GetComponentInParent<ControlPj>().isActiveAndEnabled)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                StopAllCoroutines();
                StartCoroutine(AgrandarObjeto(2f));
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                StopAllCoroutines();
                StartCoroutine(AchicarObjeto(2f));
                transform.localScale = transformInicial.localScale;

            }
        }

    }
    IEnumerator AgrandarObjeto(float duracion)
    {
        float tiempoPasado = 0;
        Vector3 escalaInicial = transform.localScale;
        Vector3 escalaObjetivo = new Vector3(10f, 15f, 1f);

        while (tiempoPasado < duracion)
        {
            tiempoPasado += Time.deltaTime;
            float t = tiempoPasado / duracion;
            transform.localScale = Vector3.Lerp(escalaInicial, escalaObjetivo, t);
            yield return null;
        }
    }
    IEnumerator AchicarObjeto(float duracion)
    {
        float tiempoPasado = 0;
        Vector3 escalaInicial = transform.localScale;
        Vector3 escalaObjetivo = new Vector3(0.4f, 0.4f, 1f);

        while (tiempoPasado < duracion)
        {
            tiempoPasado += Time.deltaTime;
            float t = tiempoPasado / duracion;
            transform.localScale = Vector3.Lerp(escalaInicial, escalaObjetivo, t);
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "EnemigoInvisible")
        {
            print(other.name);
            sprite = other.GetComponent<SpriteRenderer>();
            sprite.enabled = true;
            Invoke("Activar", 10);
        }
    }
    void Activar()
    {
        sprite.enabled = false;
    }

}
