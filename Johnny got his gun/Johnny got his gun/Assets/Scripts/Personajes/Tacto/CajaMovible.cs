using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CajaMovible : MonoBehaviour
{
    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.drag = 5f;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Tacto"))
        {
            rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Tacto"))
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }
}
