using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebote : MonoBehaviour
{
    [SerializeField] private float velRebote;
    private Rigidbody2D rb;
    void Start()
    {
        rb=GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        
    }
    public void Rebotar()
    {
        rb.velocity = new Vector2(rb.velocity.x, velRebote);
    }
}
