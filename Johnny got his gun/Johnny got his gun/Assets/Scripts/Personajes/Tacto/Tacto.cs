using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tacto : MonoBehaviour
{
    public Collider2D triggerIzquierdo;
    public Collider2D triggerDerecho;
    private Rigidbody2D rb;
    private bool dere;
    private bool izq;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        triggerIzquierdo = transform.Find("TriggerIzquierdo").GetComponent<Collider2D>();
        triggerDerecho = transform.Find("TriggerDerecho").GetComponent<Collider2D>();
    }
    private void Update()
    {
        if (GetComponentInParent<ControlPj>().isActiveAndEnabled)
        {
            if (izq && Input.GetKeyDown(KeyCode.D))
            {
                rb.constraints = RigidbodyConstraints2D.None;
                izq = false;
            }
            if (dere && Input.GetKeyDown(KeyCode.A))
            {
                rb.constraints = RigidbodyConstraints2D.None;
                dere = false;
            }
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Stickeable"))
        {
            if (other.transform.position.x < transform.position.x)
            {
                Debug.Log("Trigger del lado izquierdo activado");
                rb.constraints = RigidbodyConstraints2D.FreezePositionY;
                izq = true;
            }
            else
            {
                Debug.Log("Trigger del lado derecho activado");
                rb.constraints = RigidbodyConstraints2D.FreezePositionY;
                dere = true;
            }
        }
    }

    IEnumerator Cooldown(float time)
    {
        yield return new WaitForSeconds(time);
    }
}



