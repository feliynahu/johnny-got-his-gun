using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour
{
    //[SerializeField] Transform light;

    Vector3 targetRotation;
    Vector3 MousePositionInicial;
  
    void Start()
    {
        MousePositionInicial = Input.mousePosition;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<ControlPj>().isActiveAndEnabled)
        {
            Vector3 mousePos = Camera.main.WorldToViewportPoint(transform.position);
            Vector3 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

            Vector3 lookPos = mouseOnScreen - mousePos;
            float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg - 90f;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }
}

